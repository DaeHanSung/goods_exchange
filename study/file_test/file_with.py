#  with open("resource/foo.txt", 'w') as f:  # open이라는 함수가 성공하게 되면 f라는 변수를 생성하고
#   f.write("Life is too short, you need python")  # 생성된 f에다가 "Life is too short, you need python"이 문구를 쓴다

with open('resource/testfile.txt', 'r') as file_date:
    for line in file_date:
        print(line)
