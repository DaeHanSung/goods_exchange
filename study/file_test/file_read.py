# 파일 읽기 테스트
f = open("resource/testfile.txt", 'r')  # resource 폴더안의 testfile.txt 파일을 읽기모드로 연다
while True:  # 의미 없이 반복한다.
    line = f.readline()  # resource 폴더안의 testfile.txt 파일에서 문장을 한 줄씩 읽어드린다.
    if not line:  # 더 이상 읽을줄이 없으면
        break  # 루프를 빠져나간다.
    print(line)  # 라인을 출력한다.
f.close()  # resource 폴더안의 testfile.txt 파일을 닫는다
