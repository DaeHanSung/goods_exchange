# 데이터 쓰기
f = open("resource/testfile.txt", 'w')  # resource 폴더안의 testfile.txt 파일을 쓰기모드로 연다
for i in range(1, 11):  # for 반복문을 이용하여 1부터 10까지 i에 순서대로 대입.
    data = str(i) + "번째 줄입니다.\n"  # i에 순서대로 대입한 숫자를 str(i)를 사용하여 "번째 줄입니다" 같이 저장한다.
    f.write(data)  # data에 저장되어 있는 "str(i) + "번째 줄입니다." 를 화면에 쓴다.
f.close()  # resource 폴더안의 testfile.txt 파일을 닫는다.
