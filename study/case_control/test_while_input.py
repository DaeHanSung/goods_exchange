num = int(input('값을 입력해 주세요 : '))  # num 이라는 변수에 input 함수를 통해 숫자를 넣는다.

i = 0  # i라는 변수에 0을 대입한다.
while i < num:  # i가 bum 보다 작을 동안에
    print('', num)  # 한 칸을 띄우고 num에 입력한 숫자를 출력한다.
    i = i + 1  # i는 1개씩 증가한다.
