height = 100  # height 라는 변수에 100을 대입한다.
bounce = 3 / 5  # bounce 라는 변수에 3 / 5 을 대입한다.

a = 1  # a라는 변수에 1을 대입한다.

while a <= 10:  # a가 10에 크거나 같을때 동안
    height = height * bounce  # height * bounce를 계산하고
    print(f'{a} {round(height, 4)}')  # a의 값과 round라는 함수로  소수 4자리 까지 반올림 한 값을 출력한다.
    a = a + 1  # a는 1씩 증가한다.
