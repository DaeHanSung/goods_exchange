# readline_test.py
f = open("resources/testfile.txt", 'r')
while True:
    line = f.readline()
    if not line: break
    print(line)
f.close()
